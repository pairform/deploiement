#Etape de déploiement serveur
---

La migration des applications et des fichiers de configuration a été
effectuée avec git et scp du Serveur en production vers une machine de
test virtuel tournant sur debian8.

### 1) Installer sudo

> apt-get install sudo

### 2) Installer SSH

> apt-get install openssh-server

### 3) Mise à jour des paquets

> sudo apt-get update && sudo apt-get upgrade

### 4) Configurer une IP fixe de la machine virtuelle

> Vi /etc/network/interfaces
>
> auto eth0
>
> iface eth0 inet static
>
> address 172.21.10.104
>
> netmask 255.255.255.0
>
> gateway 172.21.10.1

### 5) Création d’un compte utilisateur admin sudoeur

> useradd -p admin -d /home/admin -s /bin/bash admin

### 6) Redéfinir le mot de passe de l’utilisateur admin

> passwd admin

### 7) Ajouter l’utilisateur au groupe sudo

> usermod –aG sudo admin

### 8) Création d’un compte utilisateur node sans droit spécifique

> useradd -p Node -d /home/node-s /bin/bash node

### 9) Redéfinir le mot de passe de l’utilisateur node

> passwd node

### 10) Sécuriser le compte root et modifier PermitRootLogin yes à no

> vi /etc/ssh/sshd\_config

### 11) Redemarrer le service ssh

> sudo service ssh restart

### 12) Installer git

> sudo apt-get install git

### 13) Installer apache puis de reconfigurer

> sudo apt-get install apache2

### 14) Migration des SSL dans le dossier ssl/

> /etc/apache2/ssl/
> certificate-166502.crt
> certificate-56113.crt
> GandiStandardSSLCA2.pem
> GandiStandardSSLCA.pem
> pairform.fr.csr
> pairform.fr.key
> sha2pairform.fr.csr
> sha2pairform.fr.key

### 15)  Migration du dossier ssl\_pairform\_wildcard/

> /etc/apache2/ssl\_pairform\_wildcard/
> certificate-452466.crt
> GandiStandardSSLCA2.pem
> pairform.fr.key

### 16) Reconfigurer les clés fichier de conf 
-> NameVirtualHost et définir les chemins des clés SSL.

> vi /etc/apache2/sites-available/beta.pairform.fr.conf
> vi /etc/apache2/sites-available/0003\_any\_443\_pairform.fr.conf
> vi /etc/apache2/sites-available/redirects\_pairform.fr.conf
> vi /etc/apache2/sites-available/static.pairform.fr.conf
> NameVirtualHost 172.21.10.104:80
> NameVirtualHost 172.21.10.104:443

### 17) Installer php5

> sudo apt-get install php5

-   Configurer php5 afin de modifier la taille de l’importation de la
    base de données, car la taille d’importation par défaut est
    trop petite.

La taille de « post\_max\_size » doit être plus grand que
« upload\_max\_filesize »

> vi /etc/php5/apache2/php.ini
> upload\_max\_filesize = 200M
> post\_max\_size = 300M

### 18) Installer mysql

> sudo apt-get install mysql-server

-   Vérifier la connexion et modifier le mot de passe de l’utilisateur
    root

> sudo mysql -u root –p
>
> mysql&gt; flush privileges;
>
> mysql&gt; GRANT ALL PRIVILEGES ON \*.\* TO root@localhost IDENTIFIED
> BY 'mot\_de\_passe' WITH GRANT OPTION;

### 19) Installer phpmyadmin

sudo apt-get install phpmyadmin

-   Importation de la base de données, configuration et création d’un
    utilisateur mysql.

> J’ai accédé à la base de données en interface graphique 
> puis j’ai importé la base de données de PairForm et crée un
> utilisateur « mysql » avec des droits uniquement sur la base de données
> importée.

![](media/image1.png){width="6.494981408573929in"
height="4.210668197725284in"}

### 20) Redémarrage des services

> sudo service mysql restart && sudo service apache2 restart

### 21) Faire un clone de l’application PairForm 
De bitbucket vers la machine virtuelle dans /var/www/PairForm/

> git clone https://bitbucket.org/pairform/back-end.git

### 22) Migrer l’application Prerender 
et la / les documents de l’application (pairform\_data) qui se trouve sur le serveur de production vers la machine virtuelle dans le dossier /var/www/PairForm

### 23) Au cas où, supprimer les paquets dans « node\_modules » qui se trouve dans
    l’application PairForm, et « node\_modules » dans l’application
    Prerender car les paquets qui sont installés (Mac OS X) n’est pas
    compatible avec le nouveau serveur Debian.

> sudo rm /var/www/PairForm/back-end/node\_modules/\*
>
> sudo rm /var/www/PairForm/prerender/node\_modules/\*

### 24) Installation des services suivants : make, g++

sudo apt-get install make

sudo apt-get install g++

### 25) Installer «npm » qui permet d’installer des paquets pour nodejs puis
    «grunt » qui est un prérequis de node.

> sudo npm install
>
> sudo npm install –g grunt

### 26) Installer les services « Node »

> sudo apt install –y nodejs

### 27) Configurer le fichier de configuration de PairForm

Configurer l’accès à la base de données.

> vi /var/www/PairForm/back-end/config/default.yaml

Ajouter :

host : ‘’localhost’’

le nom de la base de données : "PairForm\_v1\_v2\_400"

L’utilisateur qui se connecter à la base de données : "root"

Le mot de passe de l’utilisateur : ‘mot\_de\_passe’’

-  Reconfigurer les adresses et urls dans local.yaml

> # adresses et urls
> urls:
> serveur: "http://localhost"
> serveur\_node: "http://172.21.10.104:3000"
> alias\_serveur: "http://localhost"
> prerender: "http://172.21.10.104:2999"
> repertoire\_upload: "/var/www/PairForm/pairform\_data"

### 28) Définir des droits à la base de données (PairForm\_v1\_2\_400)

> sudo chown mysql:mysql /var/lib/mysql/PairForm\_v1\_2\_400

### 29) Définir des droits à pour l’utilisateur node car c’est l’utilisateur node qui exécute Pairform et Prérender.

> sudo chown -R node:admin /var/www/PairForm/back-end/dist/
> sudo chown -R node:admin logs
> sudo chown node:admin newrelic\_agent.log

-   « ERREUR » Il se peut que debian8 soit sensible à la casse
    contrairement à Mac OS X. Lors du lancement de l’application, il y
    aura certainement une erreur ou il va falloir modifier le fichier de
    configuration où soit renommer dossier.

> cd /var/www/PairForm/back-end/pairform\_modules/webservices/lib/backoffice/entites
> mv openBadge.js openbadge.js

### 30) Installer forever afin de pouvoir exécuter PairForm et Prerender automatiquement même après un redemarrage de serveur.

> sudo npm install forever –g
> sudo npm install -g forever-service

-   Créer un service nommé (pairform) qui sera lancé par l’utilisateur
    node et lancera automatiquement l’application même après un
    redémarrage du serveur.

> cd /var/www/PairForm/back-end/
>
> sudo forever-service install -s \`pwd\`/server.js -r node -f " -o \`pwd\`/logs/out.log -e \`pwd\`/logs/err.log" -e "NODE\_LOGGER\_LEVEL=info NODE\_CWD=\`pwd\` NODE\_SERVE\_STATIC=0 NODE\_CLUSTERED=1 NODE\_LAUNCH\_SCRIPT=server.js NODE\_CONFIG\_DIR=\`pwd\`/config NODE\_LOG\_DIR=\`pwd\`/logs NODE\_LOGGER\_PLUGIN=utilNODE\_ENV=production NODE\_LOGGER\_GRANULARLEVELS=0 NODE\_HOT\_RELOAD=0 NODE\_CONFIG\_DISABLE\_FILE\_WATCH=Y" pairform

Lancer le service :

> sudo service pairform start

Vérifier si la commande est activée :

> sudo –u node forever list

Les logs du service pairform :

> tail -f /var/log/pairform.log

##Prerender

Créer un service nommé (pairform-prerender) qui sera aussi lancé par l’utilisateur node.

> cd /var/www/PairForm/prerender/
> sudo forever-service install -s \`pwd\`/server.js -r node -e
"PRERENDER\_NUM\_WORKERS=\`grep -c \^processor /proc/cpuinfo\`
PORT=2999" pairform-prerender

Lancer le service :

> sudo service pairform-prerender start

Vérifier si la commande est activée :

> sudo –u node forever list

Les logs du service pairform-prerender:

> tail -f /var/log/pairform-prerender.log

Lors du redémarrage du serveur, Pairform et Prerender vont se lancer
automatiquement.


##Installer Fail2ban

> sudo apt-get install fail2ban

-   Configurer fail2ban

Vi /etc/fail2ban/jail.conf

Modifier : action = %(action\_XXX)s  ==&gt; action = %(action\_mwl)s ,
afin d’obtenir plus d’informations dans les rapports envoyés par
Fail2ban.

-   Sécuriser le daemon SSH

Modifier le paramettre enable afin de mettre sur true. Si le service ne nous interesse plus, il suffit de mettre false.

>\[ssh\]
>enabled = true
>port = ssh
>filter = sshd
>action = iptables\[name=SSH, port=ssh, protocol=tcp\]
>logpath = /var/log/auth.log
>maxretry = 6
>bantime =3600

Références :
*https://blog.nicolargo.com/2012/02/proteger-son-serveur-en-utilisant-fail2ban.html*
*https://wiki.debian-fr.xyz/Fail2ban*

##Script de backup de base de données

> \#!/bin/bash
> 
> \# Chemin de sauvegarde
> 
> BackupFolder='/Volumes/Promise1/ServiceData/PairForm/backup\_bdd\_Pairform/'
> 
> \# Prefixe du nom du fichier de sauvegarde (avant le nom de la base)
> 
> PREFIXE=db.
> 
> \# Suffixe du nom du fichier de sauvegarde (après le nom de la base)
> 
> SUFFIXE=\`date +%Y-%m-%d-%H:%M\`.sql
> 
> mysqldump -h podcast.mines-nantes.fr -u root –p « mot-passe»
> PairForm\_v1\_2\_400 &gt; \$BackupFolder/\$PREFIXE\$i\$SUFFIXE;
> 
> #Supprimer les fichiers de plus d'un mois
> 
> find /Volumes/Promise1/ServiceData/PairForm/backup\_bdd\_Pairform/ -name
> "\*.sql" -mtime +30 -exec rm {} \\;
