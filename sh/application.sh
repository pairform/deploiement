#!/bin/bash
read  -p "Voulez vous migrer l'application PairForm ? Taper  "Y" ou "N" " migration_pairform
case $migration_pairform in
[Yy]* ) 
	read -p "Entrez un le chemin de la migration : " chemin_migration_pairform
	echo -e "\033[1;32mPairform sera migré à l'emplacement : $chemin_migration_pairform \033[0m "
	if [ "`$chemin_migration_pairform`" != "" ]
		then
			echo -e "\033[1;31mL'emplacement $chemin_migration_pairform existe déjà \033[0m"
		else
			echo "$chemin_migration_pairform n'existe pas"

			echo -e "\033[1;32mCréation de l'emplacement de PairForm à l'emplacement : $chemin_migration_pairform  \033[0m"
			sudo mkdir $chemin_migration_pairform
	#echo -e "\033[1;32mLe chemin à été créé. \033[0m"	
			echo -e "\033[1;32mMigration de PairForm \033[0m"
			cd $chemin_migration_pairform
			sudo touch test.txt 
			git clone https://teddy-benjamin@bitbucket.org/pairform/back-end.git
			sudo chown -R utilisateur_node:admin $chemin_migration_pairform		
			cd $HOME

	fi
	;;
	#Si on ne veut pas créer d'utilisateur la commande se termine
[Nn]* ) ;;

* ) echo -e "\033[1;31mERREUR Taper Y ou N \033[0m"
exit;;
esac
