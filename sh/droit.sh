#!/bin/bash
if [ "`$chemin_migration_pairform`" != "" ]
        then
            echo -e  "\033[1;32mDéfinition des droits sur l'application \033[0m"
		sudo chown -R $utilisateur_node:adm $chemin_migration_pairform/back-end/dist/
		sudo chown -R $utilisateur_node:adm $chemin_migration_pairform/back-end/logs/
                sudo chown -R $utilisateur_node:adm $chemin_migration_pairform/back-end/newrelic_agent.log/

       else
           echo -e  "\033[1;31mAucun chemin de migration trouvé \033[0m"
           
fi






#echo "Définir des droits à pour l’utilisateur node car c’est l’utilisateur node qui exécute Pairform et Prérender."
#sudo chown -R node:adm $chemin_migration_pairform/back-end/dist/
#sudo chown -R node:adm logs
#sudo chown node:teddy newrelic_agent.log

