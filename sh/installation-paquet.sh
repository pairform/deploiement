#!/bin/bash
#Installation de git
if [ "`which git`" != "" ]
        then
            echo -e  "\033[1;32mGit est déja installé \033[0m"
       else
           echo -e  "\033[1;32mInstallation de git \033[0m"
	sudo apt-get install git
fi


#Installation de php5
if [ "`which php5`" != "" ]
        then
            echo -e  "\033[1;32mPhp5 est déja installé \033[0m"
		echo -e "\033[1;31mCONFIGURER post_max_size ET upload_max_filesize DANS LE FICHIER /etc/php5/apache2/php.init AFIN D'AUGMENTER LA TAILLE POUR IMPORTER LA BASE DE DONNEES \033[0m"
       else
        	echo -e  "\033[1;32mInstallation de php5 \033[0m"
		sudo apt-get install php5
		echo -e "\033[1;31mCONFIGURER post_max_size ET upload_max_filesize DANS LE FICHIER /etc/php5/apache2/php.init AFIN D'AUGMENTER LA TAILLE POUR IMPORTER LA BASE DE DONNEES \033[0m"
fi

#Installation de mysql 
if [ "`which mysql`" != "" ]
        then
            echo -e  "\033[1;32mMysql est déja installé \033[0m"
       else
                echo -e  "\033[1;32mInstallation de mysql \033[0m"
sudo apt-get install mysql-server
fi

#Installation d'apache2 
if [ "`sudo service --status-all | grep apache2`" != "" ]
       then
            	echo -e  "\033[1;32mApache2 est déja installé \033[0m"
       else
                echo -e  "\033[1;32mInstallation de Aapache2 \033[0m"
		sudo apt-get install apache2
fi

