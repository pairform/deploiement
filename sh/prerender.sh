#!/bin/bash


 ################################################
 #                                              #
 # Création de l'utilisateur exécutant PairForm #
 #                                              #
 ###############################################
 # Demande de saisie d'un utilisateur exécutant PairForm, si l'utiisateur existe déja alors le script dit que l'utilisateur est déja créé, sinon il propose la saisie d'un utilisateur exécutant PairForm et un mot de passse
 
 
 read  -p "Voulez vous créer un utilisateur exécutant PairForm ? Taper  "Y" ou "N" " nouveau_utilisateur_node
 case $nouveau_utilisateur_node in
    [Yy]* )
 
         read -p "Entrez un nom d'utilisateur exécutant PairForm : " utilisateur_node
 
         if [ "`cat /etc/passwd | grep $utilisateur_node`" != "" ]
         then
                 echo "L'utilisateur $utilisateur_node existe déjà"
         else
                 echo "$utilisateur_node n'existe pas"
 
                 #Créer un utilisateur avec un mot de passe
                 echo -e "\033[1;32mCréation de l'utilisateur $utilisateur_node \033[0m"
                 sudo useradd -p $utilisateur_node -d /home/$utilisateur_node -s /bin/bash $utilisateur_node
                 echo -e "\033[1;31mTaper le mot de passe de l'utilisateur \033[0m"
                 sudo passwd $utilisateur_node
 
         fi
         ;;
 
    #Si on ne veut pas créer d'utilisateur la commande se termine
    [Nn]* ) ;;
 
    * ) echo -e "\033[1;31mERREUR Taper Y ou N \033[0m"
         exit;;
 esac



 ###########################################################
 #                                                         #
 #  Migration à un Emplacement de Prerender		   #
 #                                                         #
 ###########################################################

 read  -p "Voulez vous migrer Prerender ? Taper  "Y" ou "N" " migration_prerender
 case $migration_prerender in
 [Yy]* )

#Prerender sera migrer au meme emplacement que l'application PairForm
         echo -e "\033[1;32mPrerender sera migré au même emplacement que PairForm : $chemin_migration_pairform \033[0m "

                        echo -e "\033[1;32mMigration de Prerender \033[0m"
                         cd $chemin_migration_pairform/
                         
			sudo git clone https://github.com/prerender/prerender.git

			sudo chown -R $utilisateur_node:adm $chemin_migration_pairform/prerender
                        cd $chemin_migration_pairform/prerender/
                        echo -e "\033[1;32mInstallation des paquets installé dans node_modules \033[0m"
                        sudo npm install
                        sudo chown -R $utilisateur_node:adm $chemin_migration_pairform/prerender/
			echo -e "\033[1;32mLes paquets de Prerender ont été installés dans node_modules avec npm \033[0m"

                        cd $HOME
         ;;
         #Si on ne veut pas migrer l'application, le sripte continu

 [Nn]* ) ;;

 * ) echo -e "\033[1;31mERREUR Taper Y ou N \033[0m"
 exit;;
 esac

