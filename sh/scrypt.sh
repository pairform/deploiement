#!/bin/bash

#Installer sudo 
if ! [ -x "$(command -v sudo)" ]
then
  echo -e "\033[1;31mSudo non installé : connexion en root pour installation de sudo obligatoire. \033[0m"
  echo -e "\033[1;31mInstaller les paquets (apt-get update && apt-get upgrade) \033[0m"
  echo -e "\033[1;31mInstaller Sudo "apt-get install sudo" \033[0m"


    #Logout du root
  exit
fi


# Si SSH  est déja installer alors il continue le script sinon il installe le paquet

if [ "`which ssh`" != "" ]
        then
            echo -e  "\033[1;32mOpenssh-server est déja installé \033[0m"
       else
           echo -e  "\033[1;32mInstallation de SSH \033[0m"
           sudo apt-get openssh-server
fi



#########################################
# 	                                #
# Création d'un utilisateur de Pairform #
#                                       #
#########################################
#  Demande de saisie d'un utilisateur, si l'utilisateur n'existe pas alors le créer et créer son mot de passe, sinon dire qu'il existe et quitter 


read  -p "Voulez vous créer un utilisateur Sudoeur? Taper  "Y" ou "N" " nouveau_utilisateur
case $nouveau_utilisateur in
   [Yy]* )

	read -p "Entrez un nom d'utilisateur principal : " utilisateur_principal

	if [ "`cat /etc/passwd | grep $utilisateur_principal`" != "" ]
	then
    		echo "L'utilisateur $utilisateur_principal existe déjà"
	else
    		echo "$utilisateur_principal n'existe pas" 

		#Créer un utilisateur avec un mot de passe
		echo -e "\033[1;32mCréation de l'utilisateur $utilisateur_principal \033[0m"
	    	sudo useradd -p $utilisateur_principal -s /bin/bash -m $utilisateur_principal
   	 	echo -e "\033[1;31mTaper le mot de passe de l'utilisateur \033[0m"
    		sudo passwd $utilisateur_principal

		#Ajout de l'utilisateur au groupe sudo 
		echo -e "\033[1;31mAjout de l'utilisateur au groupe sudo \033[0m"
		sudo usermod -aG sudo $utilisateur_principal

	fi
	;;

		#Si on ne veut pas créer d'utilisateur la commande se termine	
   [Nn]* ) ;;

    * ) echo -e "\033[1;31mERREUR Taper Y ou N \033[0m" 
	exit;;	
esac	


################################################
#                                              #
# Création de l'utilisateur exécutant PairForm #
#                                              #
###############################################
# Demande de saisie d'un utilisateur exécutant PairForm, si l'utiisateur existe déja alors le script dit que l'utilisateur est déja créé, sinon il propose la saisie d'un utilisateur exécutant PairForm et un mot de passse 


read  -p "Voulez vous créer un utilisateur exécutant PairForm ? Taper  "Y" ou "N" " nouveau_utilisateur_node
case $nouveau_utilisateur_node in
   [Yy]* )

        read -p "Entrez un nom d'utilisateur exécutant PairForm : " utilisateur_node

        if [ "`cat /etc/passwd | grep $utilisateur_node`" != "" ]
        then
                echo -e "\033[1;31mL'utilisateur $utilisateur_node existe déjà \033[0m"
        else
                echo -e "033[1;31m$utilisateur_node n'existe pas \033[0m"

		#Créer un utilisateur avec un mot de passe
        	echo -e "\033[1;32mCréation de l'utilisateur $utilisateur_node \033[0m"
        	sudo useradd -p $utilisateur_node -s /bin/bash -m $utilisateur_node
        	echo -e "\033[1;31mTaper le mot de passe de l'utilisateur \033[0m"
        	sudo passwd $utilisateur_node

	fi
	;;

   #Si on ne veut pas créer d'utilisateur la commande se termine
   [Nn]* ) ;;

   * ) echo -e "\033[1;31mERREUR Taper Y ou N \033[0m"
	exit;;
esac



###########################
#                         #
# Installation de paquets #
#                         #
###########################
# Si Git est déja installer alors il continue le script sinon il installe le paquet

if [ "`which git`" != "" ]
        then
            echo -e  "\033[1;32mGit est déja installé \033[0m"
       else
           echo -e  "\033[1;32mInstallation de git \033[0m"
        sudo apt-get install git
fi




#Installation d'apache2
if [ "`sudo service --status-all | grep apache2`" != "" ]
       then
                echo -e  "\033[1;32mApache2 est déja installé \033[0m"
       else
                echo -e  "\033[1;32mInstallation de Aapache2 \033[0m"
                sudo apt-get install apache2
fi



#Installation de php5
if [ "`which php5`" != "" ]
        then
            echo -e  "\033[1;32mPhp5 est déja installé \033[0m"
                echo -e "\033[1;31mSI VOUS IMPORTEZ UNE BASE DE DONNEES, CONFIGURER post_max_size ET upload_max_filesize DANS LE FICHIER /etc/php5/apache2/php.init AFIN D'AUGMENTER LA TAILLE POUR IMPORTER LA BASE DE DONNEES \033[0m"
       else
                echo -e  "\033[1;32mInstallation de php5 \033[0m"
                sudo apt-get install php5
                echo -e "\033[1;31mSI VOUS IMPORTEZ CONFIGURER post_max_size ET upload_max_filesize DANS LE FICHIER /etc/php5/apache2/php.init AFIN D'AUGMENTER LA TAILLE POUR IMPORTER LA BASE DE DONNEES \033[0m"
fi

#Installation de Mysql-server
if [ "`which mysql`" != "" ]
        then
            echo -e  "\033[1;32mMysql est déja installé \033[0m"
       else
                echo -e  "\033[1;32mInstallation de mysql \033[0m"
		sudo apt-get install mysql-server


                echo -e "\033[1;31mVeillez configurer le mot de passe de l'utilisateur root \033[0m"
                sudo mysql -u root –p 
 
fi


#Installation de PhpMyAdmin
if [ "`cat /etc/phpmyadmin`" != "" ]
        then
            echo -e  "\033[1;32mPhpMyAdmin est déja installé \033[0m"
       else
           echo -e  "\033[1;32mInstallation de PhpMyAdmin \033[0m"
           sudo apt-get install phpmyadmin
fi



echo -e "\033[1;32mRedemmarage des services MYSQL et Apache \033[0m"
sudo service mysql restart && sudo service apache2 restart



##################################################################################################


#Importation de la base de données dans l'emplacement /var/www/PairForm/
#
# mysql -u root -p < PairFormForm_v1_2_400 





####################################################################################################

#Installation de Make
if [ "`which make`" != "" ]
        then
            echo -e  "\033[1;32mMake est déja installé \033[0m"
       else
                echo -e  "\033[1;32mInstallation de Make \033[0m"
                sudo apt-get install make
fi


#Installation de G++
if [ "`which g++`" != "" ]
        then
            echo -e  "\033[1;32mG++ est déja installé \033[0m"
       else
                echo -e  "\033[1;32mInstallation de G++ \033[0m"
                sudo apt-get install g++
fi




#Installation de curl
if [ "`which curl`" != "" ]
        then
            echo -e  "\033[1;32mCurl est déja installé \033[0m"
       else
                echo -e  "\033[1;32mInstallation de Curl \033[0m"
                sudo apt-get install curl libcurl3 php5-curl
fi


#Installation de nodejs
if [ "`which nodejs`" != "" ]
        then 
            echo -e  "\033[1;32mNodejs est déja installé \033[0m"
       else
                echo -e  "\033[1;32mInstallation de Nodejs \033[0m"
		sudo apt-get install -y nodejs
fi

#Installation de Grunt
if [ "`which grunt`" != "" ]
        then
            echo -e  "\033[1;32mGrunt est déja installé \033[0m"
       else
                echo -e  "\033[1;32mInstallation de Grunt. \033[0m"
                sudo npm install -g grunt
fi


 ####################################################################
 #                                                                  #
 #  Déploiement à un emplacement précis de l'application PairForm   #
 #                                                                  #
 ####################################################################

read  -p "Voulez vous deployer l'application PairForm ? Taper  "Y" ou "N" " deploiement_pairform
case $deploiement_pairform in
[Yy]* )
        read -p "Entrez l'emplacement de destination de la deploiement : " chemin_deploiement_pairform
        echo -e "\033[1;32mPairform sera deployé à l'emplacement : $chemin_deploiement_pairform \033[0m "
        if [ "`$chemin_deploiement_pairform`" != "" ]
                then
                        echo -e "\033[1;31mL'emplacement $chemin_deploiement_pairform existe déjà \033[0m"
                else
                        echo "$chemin_deploiement_pairform n'existe pas"
                        echo -e "\033[1;32mCréation de l'emplacement de PairForm à l'emplacement : $chemin_deploiement_pairform  \033[0m"
                        sudo mkdir $chemin_deploiement_pairform
                       sudo chown -R $utilisateur_node:adm $chemin_deploiement_pairform
                       echo -e "\033[1;32mDeployation de PairForm \033[0m"
                        cd $chemin_deploiement_pairform/
                       sudo git clone https://teddy-benjamin@bitbucket.org/pairform/back-end.git
                       sudo chown -R $utilisateur_node:adm $chemin_deploiement_pairform/back-end

                       cd $chemin_deploiement_pairform/back-end/


                       echo -e "\033[1;32mInstallation des paquets installé dans node_modules \033[0m"
	          	sudo -u $utilisateur_node npm install
		
                       echo -e "\033[1;32mMise à jour de npm \033[0m"
                  sudo npm i -g npm
                       sudo chown -R $utilisateur_node:adm $chemin_deploiement_pairform/back-end/node_modules/
                       #sudo chown -R $utilisateur_node:adm $chemin_deploiement_pairform/back-end/node_modules/@newrelic/native-metrics/
                       echo -e "\033[1;32mLes paquets ont été installés dans node_modules avec npm \033[0m"
		      sudo chown -R $utilisateur_node:adm $chemin_deploiement_pairform/back-end/node_modules 
        fi

        ;;
        #Si on ne veut pas deployer l'application, le sripte continu
[Nn]* ) ;;
* ) echo -e "\033[1;31mERREUR Taper Y ou N \033[0m"
exit;;
esac







#Défnir les droits a la base de données
#sudo chown mysql:mysql /var/lib/mysql/PairForm_v1_2_400




 ############################
 #                             #
 #  Déploiement de  Prerender  #
 #                             #
 ############################

 read  -p "Voulez vous deployer Prerender ? Taper  "Y" ou "N" " deploiement_prerender
 case $deploiement_prerender in
 [Yy]* )

#Prerender sera deployer au meme emplacement que l'application PairForm
         echo -e "\033[1;32mPrerender sera deployé au même emplacement que PairForm : $chemin_deploiement_pairform \033[0m "

                        echo -e "\033[1;32mDeployation de Prerender \033[0m"
                         cd $chemin_deploiement_pairform/

                        sudo git clone https://github.com/prerender/prerender.git

                        sudo chown -R $utilisateur_node:adm $chemin_deploiement_pairform/prerender
                        cd $chemin_deploiement_pairform/prerender/
                        echo -e "\033[1;32mInstallation des paquets installé dans node_modules \033[0m"
                        sudo npm install
                        sudo chown -R $utilisateur_node:adm $chemin_deploiement_pairform/prerender/
                        echo -e "\033[1;32mLes paquets de Prerender ont été installés dans node_modules avec npm \033[0m"

                        cd $HOME
         ;;
         #Si on ne veut pas deployer l'application, le sripte continu

 [Nn]* ) ;;

 * ) echo -e "\033[1;31mERREUR Taper Y ou N \033[0m"
 exit;;
 esac

###############################################################################

#Cette partie ne sert plus a rien car les droits sont défini a chaque fois

#echo "Définir des droits à pour l’utilisateur node car c’est l’utilisateur node qui exécute Pairform et Prérender."
#sudo chown -R node:adm $chemin_deploiement_pairform/back-end/dist/
#sudo chown -R node:adm logs
#sudo chown node:teddy newrelic_agent.log 





 
#Installer forever afin de pouvoir exécuter PairForm et Prerender automatiquement même après un redemarrage de serveur.
 #Installation de forever
 if [ "`which forever`" != "" ]
         then
             echo -e  "\033[1;32mForever est déja installé \033[0m"
        else
                 echo -e  "\033[1;32mInstallation de Forever \033[0m"
		 sudo npm install forever -g
 fi


#Installation de forever-service
if [ "`which forever-service`" != "" ]
        then
            echo -e  "\033[1;32mForever-service est déja installé \033[0m"
       else
                echo -e  "\033[1;32mInstallation de Forever-service \033[0m"
		sudo npm install -g forever-service
fi              



 ###########################################################
 #                                                         #
 #      Création du service service-prerender
 #                                                         #
 ###########################################################


read  -p "Voulez vous Créer un service PairForm afin d'exécuter l'application automatiquement ? Taper  "Y" ou "N" " service_pairform
case $service_pairform in
  [Yy]* )

	cd $chemin_deploiement_pairform/prerender/


	echo -e "\033[1;32mCréation d'un service nommé (pairform-prerender) qui sera lancé par l'utilisateur node \033[0m"
	cd /prerender/
	sudo forever-service install -s `pwd`/server.js -r $utilisateur_node -e "PRERENDER_NUM_WORKERS=`grep -c ^processor /proc/cpuinfo` PORT=2999" pairform-prerender




	echo -e "\033[1;32mLancement du service (pairform-prerender) précédemment crée \033[0m"
	sudo service pairform-prerender start
         
	;;
         #Si on ne veut pas deployer l'application, le sripte continu
         
  [Nn]* ) ;;
 
   * ) 
	echo -e "\033[1;31mERREUR Taper Y ou N \033[0m"
 	exit;;
 esac


echo -e "\033[1;32mVérification de l'activation des services \033[0m"
sudo -u $utilisateur_node forever list

################################################################



echo -e "\033[1;32mCréation d'un service nommé (pairform) qui sera lancé par l'utilisateur exécutant PairForm \033[0m"
#sudo forever-service install -s `pwd`/server.js -r node -f " -o `pwd`/logs/out.log -e `pwd`/logs/err.log" -e "NODE_LOGGER_LEVEL=info NODE_CWD=`pwd` NODE_SERVE_STATIC=0 NODE_CLUSTERED=1 NODE_LAUNCH_SCRIPT=server.js NODE_CONFIG_DIR=`pwd`/config NODE_LOG_DIR=`pwd`/logs NODE_LOGGER_PLUGIN=util NODE_ENV=production NODE_LOGGER_GRANULARLEVELS=0 NODE_HOT_RELOAD=0 NODE_CONFIG_DISABLE_FILE_WATCH=Y" pairform

