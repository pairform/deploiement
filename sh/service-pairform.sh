#!/bin/bash


################################################
#                                              #
# Création de l'utilisateur exécutant PairForm #
#                                              #
###############################################
# Demande de saisie d'un utilisateur exécutant PairForm, si l'utiisateur existe déja alors le script dit que l'utilisateur est déja créé, sinon il propose la saisie d'un utilisateur exécutant PairForm et un mot de passse
 
 
read  -p "Voulez vous créer un utilisateur exécutant PairForm ? Taper  "Y" ou "N" " nouveau_utilisateur_node
 case $nouveau_utilisateur_node in
    [Yy]* )
 
         read -p "Entrez un nom d'utilisateur exécutant PairForm : " utilisateur_node
 
         if [ "`cat /etc/passwd | grep $utilisateur_node`" != "" ]
         then
                 echo "L'utilisateur $utilisateur_node existe déjà"
         else
                 echo "$utilisateur_node n'existe pas"
 
                 #Créer un utilisateur avec un mot de passe
                 echo -e "\033[1;32mCréation de l'utilisateur $utilisateur_node \033[0m"
                 sudo useradd -p $utilisateur_node -s /bin/bash -m $utilisateur_node
                 echo -e "\033[1;31mTaper le mot de passe de l'utilisateur \033[0m"
                 sudo passwd $utilisateur_node
 
         fi
         ;;
 
    #Si on ne veut pas créer d'utilisateur la commande se termine
    [Nn]* ) ;;
 
   * ) echo -e "\033[1;31mERREUR Taper Y ou N \033[0m"
         exit;;
esac



###########################################################
#                                                         #
#  Creation du service   service-pairform
#                                                         #
###########################################################




read  -p "Voulez vous Créer un service PairForm afin d'exécuter l'application automaquement ? Taper  "Y" ou "N" " service_pairform
case $service_pairform in
[Yy]* )


cd $chemin_migration_pairform/back-end/
sudo mkdir $chemin_migration_pairform/back-end/logs
sudo chown $utilisateur_node:adm $chemin_migration_pairform/back-end/logs

sudo forever-service install -s `pwd`/server.js -r $utilisateur_node -f " -o `pwd`/logs/out.log -e `pwd`/logs/err.log" -e "NODE_LOGGER_LEVEL=info NODE_CWD=`pwd` NODE_SERVE_STATIC=0 NODE_CLUSTERED=1 NODE_LAUNCH_SCRIPT=server.js NODE_CONFIG_DIR=`pwd`/config NODE_LOG_DIR=`pwd`/logs NODE_LOGGER_PLUGIN=util NODE_ENV=production NODE_LOGGER_GRANULARLEVELS=0 NODE_HOT_RELOAD=0 NODE_CONFIG_DISABLE_FILE_WATCH=Y" pairform


echo -e "\033[1;32mLancement du service (pairform) précédemment crée \033[0m"
sudo service pairform start
         ;;
         #Si on ne veut pas migrer l'application, le sripte continu

 [Nn]* ) ;;

* ) echo -e "\033[1;31mERREUR Taper Y ou N \033[0m"
exit;;
esac
echo -e "\033[1;32mVérification de l'activation des services \033[0m"
sudo -u $utilisateur_node forever list
