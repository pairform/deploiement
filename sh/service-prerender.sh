#!/bin/bash


 ################################################
 #                                              #
 # Création de l'utilisateur exécutant PairForm #
 #                                              #
 ###############################################
 # Demande de saisie d'un utilisateur exécutant PairForm, si l'utiisateur existe déja alors le script dit que l'utilisateur est déja créé, sinon il propose la saisie d'un utilisateur exécutant PairForm et un mot de passse
 
 
 read  -p "Voulez vous créer un utilisateur exécutant PairForm ? Taper  "Y" ou "N" " nouveau_utilisateur_node
 case $nouveau_utilisateur_node in
    [Yy]* )
 
         read -p "Entrez un nom d'utilisateur exécutant PairForm : " utilisateur_node
 
         if [ "`cat /etc/passwd | grep $utilisateur_node`" != "" ]
         then
                 echo "L'utilisateur $utilisateur_node existe déjà"
         else
                 echo "$utilisateur_node n'existe pas"
 
                 #Créer un utilisateur avec un mot de passe
                 echo -e "\033[1;32mCréation de l'utilisateur $utilisateur_node \033[0m"
                 sudo useradd -p $utilisateur_node -s /bin/bash -m $utilisateur_node
                 echo -e "\033[1;31mTaper le mot de passe de l'utilisateur \033[0m"
                 sudo passwd $utilisateur_node
 
         fi
         ;;
 
    #Si on ne veut pas créer d'utilisateur la commande se termine
    [Nn]* ) ;;
 
   * ) echo -e "\033[1;31mERREUR Taper Y ou N \033[0m"
         exit;;
esac



 ###########################################################
 #                                                         #
 # 	Création du service service-prerender 
 #                                                         #
 ###########################################################


read  -p "Voulez vous Créer un service PairForm afin d'exécuter l'application automaquement ? Taper  "Y" ou "N" " service_pairform
case $service_pairform in
[Yy]* )

cd $chemin_migration_pairform/prerender/


echo -e "\033[1;32mCréation d'un service nommé (pairform-prerender) qui sera lancé par l'utilisateur node \033[0m"
cd /prerender/
sudo forever-service install -s `pwd`/server.js -r $utilisateur_node -e "PRERENDER_NUM_WORKERS=`grep -c ^processor /proc/cpuinfo` PORT=2999" pairform-prerender




echo -e "\033[1;32mLancement du service (pairform) précédemment crée \033[0m"
sudo service pairform-prerender start
         ;;
         #Si on ne veut pas migrer l'application, le sripte continu

 [Nn]* ) ;;

 * ) echo -e "\033[1;31mERREUR Taper Y ou N \033[0m"
 exit;;
 esac
echo -e "\033[1;32mVérification de l'activation des services \033[0m"
sudo -u $utilisateur_node forever list
cd $HOME

