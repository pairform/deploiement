#!/bin/bash


 ################################################
 #                                              #
 # Création de l'utilisateur exécutant PairForm #
 #                                              #
 ###############################################
 # Demande de saisie d'un utilisateur exécutant PairForm, si l'utiisateur existe déja alors le script dit que l'utilisateur est déja créé, sinon il propose la saisie d'un utilisateur exécutant PairForm et un mot de passse
 
 
 read  -p "Voulez vous créer un utilisateur exécutant PairForm ? Taper  "Y" ou "N" " nouveau_utilisateur_node
 case $nouveau_utilisateur_node in
    [Yy]* )
 
         read -p "Entrez un nom d'utilisateur exécutant PairForm : " utilisateur_node
 
         if [ "`cat /etc/passwd | grep $utilisateur_node`" != "" ]
         then
                 echo "L'utilisateur $utilisateur_node existe déjà"
         else
                 echo "$utilisateur_node n'existe pas"
 
                 #Créer un utilisateur avec un mot de passe
                 echo -e "\033[1;32mCréation de l'utilisateur $utilisateur_node \033[0m"
                 sudo useradd -p $utilisateur_node -d /home/$utilisateur_node -s /bin/bash $utilisateur_node
                 echo -e "\033[1;31mTaper le mot de passe de l'utilisateur \033[0m"
                 sudo passwd $utilisateur_node
 
         fi
         ;;
 
    #Si on ne veut pas créer d'utilisateur la commande se termine
    [Nn]* ) ;;
 
    * ) echo -e "\033[1;31mERREUR Taper Y ou N \033[0m"
         exit;;
 esac



 ###########################################################
 #                                                         #
 #  Migration à un Emplacement de l'application PairForm   #
 #                                                         #
 ###########################################################

 read  -p "Voulez vous migrer l'application PairForm ? Taper  "Y" ou "N" " migration_pairform
 case $migration_pairform in
 [Yy]* )
         read -p "Entrez le chemin de la migration : " chemin_migration_pairform
         echo -e "\033[1;32mPairform sera migré à l'emplacement : $chemin_migration_pairform \033[0m "
         if [ "`$chemin_migration_pairform`" != "" ]
                 then
                         echo -e "\033[1;31mL'emplacement $chemin_migration_pairform existe déjà \033[0m"
                 else
                         echo "$chemin_migration_pairform n'existe pas"

                         echo -e "\033[1;32mCréation de l'emplacement de PairForm à l'emplacement : $chemin_migration_pairform  \033[0m"
                         sudo mkdir $chemin_migration_pairform
			sudo chown -R $utilisateur_node:root $chemin_migration_pairform
                        echo -e "\033[1;32mMigration de PairForm \033[0m"
                         cd $chemin_migration_pairform/
                         
			sudo git clone https://teddy-benjamin@bitbucket.org/pairform/back-end.git

			sudo chown -R $utilisateur_node:root $chemin_migration_pairform/back-end
                        #echo -e "\033[1;32mSuppression des paquets installé dans node_modules \033[0m"
                        # sudo rm -r $chemin_migration_pairform/back-end/node_modules/*
                        cd $chemin_migration_pairform/back-end/
                        echo -e "\033[1;32mInstallation des paquets installé dans node_modules \033[0m"
                        sudo npm install
			echo -e "\033[1;32mMise à jour de npm \033[0m"
			sudo npm i -g npm
                        sudo chown -R $utilisateur_node:root $chemin_migration_pairform/back-end/node_modules/
			#sudo chown -R $utilisateur_node:root $chemin_migration_pairform/back-end/node_modules/@newrelic/native-metrics/
			echo -e "\033[1;32mLes paquets ont été installés dans node_modules avec npm \033[0m"

                         cd $HOME
         fi
         ;;
         #Si on ne veut pas migrer l'application, le sripte continu

 [Nn]* ) ;;

 * ) echo -e "\033[1;31mERREUR Taper Y ou N \033[0m"
 exit;;
 esac

