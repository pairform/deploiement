#!/bin/bash

read  -p "Voulez vous créer un utilisateur exécutant PairForm ? Taper  "Y" ou "N" " nouveau_utilisateur_node
case $nouveau_utilisateur_node in
   [Yy]* )

        read -p "Entrez un nom d'utilisateur exécutant PairForm : " utilisateur_node

        if [ "`cat /etc/passwd | grep $utilisateur_node`" != "" ]
        then
                echo "L'utilisateur $utilisateur_node existe déjà"
        else
                echo "$utilisateur_node n'existe pas"

#Créer un utilisateur avec un mot de passe
        echo -e "\033[1;32mCréation de l'utilisateur $utilisateur_node \033[0m"
        sudo useradd -p $utilisateur_node -d /home/$utilisateur_node -s /bin/bash $utilisateur_node
        echo -e "\033[1;31mTaper le mot de passe de l'utilisateur \033[0m"
        sudo passwd $utilisateur_node 

fi
;;

#Si on ne veut pas créer d'utilisateur la commande se termine
   [Nn]* );;

   * ) echo -e "\033[1;31mERREUR Taper Y ou N \033[0m";;
esac



